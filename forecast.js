//https://api.openweathermap.org/data/2.5/forecast?q=Mumbai&units=metric&appid=e47a69424870b00a3d6cf00abc8d0b77

const key = "e47a69424870b00a3d6cf00abc8d0b77";
             
const getForecast = async (city) => {
    const base = "https://api.openweathermap.org/data/2.5/forecast";
    const query = `?q=${city}&units=metric&appid=${key}`;
    
    const response = await fetch(base+query);
//     console.log(response);
    if(!response.ok)
        throw new Error("Status Code: " + response.status);
    
    const data = await response.json();
    // console.log(data);
    return data;
}
getForecast('Mumbai')
.then(data=>console.log(data))
.catch(err=>console.warn(err));